<?
error_reporting(E_ERROR | E_PARSE);
session_start();


$localDir = 'jonathanzils_projeto/';
$base_url = "http://".$_SERVER['HTTP_HOST'].'/';
$url_site = $base_url.$localDir;
$fullPath = $_SERVER['DOCUMENT_ROOT'].'/';
$fullPath .= $localDir;


$models = $fullPath.'models/';
$controllers = $fullPath.'controllers/';
$views = $fullPath.'views/';


require $models."connectDB.Class.php";
require $models."dao.Class.php";

require $models."restrito.Class.php";
require $models."cadastrarAdm.Class.php";
require $models."cadastrarCondominio.Class.php";
require $models."cadastrarBloco.Class.php";
require $models."cadastrarUnidade.Class.php";
require $models."cadastro.Class.php";
require $models."cadastrarconselho.Class.php";
require $models."cadastraUsuario.Class.php";

$nav = array(
    'home'=>'Dashboard',
    'Registrar'=> array(
        'cadastro'=>'Morador',
        'cadCondominio'=>'Condominio',
        'cadBlocos'=>"blocos",
        'cadUnidade'=>"Unidades",
        'cadConselho'=>'conselho',
        'cadAdm'=>'Administradora'

    ),
    'Visualizar'=>array(
        'clientes'=>'Moradores',
        'listaCondominios'=>'Condominios',
        'listaBloco'=>'Blocos',
        'listaUnidade'=>'Unidades',
        'listaConselho'=>'conselho',
        'listaAdm'=>'administradoras'
    ),
    'Usuarios'=>array(
        'cadUsuario'=>"cadastrar",
        'listaUsuario'=>"listar"
    )
    
);
function legivel($var,$width = '250',$height = '400') {
    echo "<pre>";
    if(is_array($var)) {
        print_r($var);
    } else {
        print($var);
    }
    echo "</pre>";
}
$estados = array(
    "AC" => "Acre", 
    "AL" => "Alagoas", 
    "AM" => "Amazonas", 
    "AP" => "Amapá",
    "BA" => "Bahia",
    "CE" => "Ceará",
    "DF" => "Distrito Federal",
    "ES" => "Espírito Santo",
    "GO" => "Goiás",
    "MA" => "Maranhão",
    "MT" => "Mato Grosso",
    "MS" => "Mato Grosso do Sul",
    "MG" => "Minas Gerais",
    "PA" => "Pará",
    "PB" => "Paraíba",
    "PR" => "Paraná",
    "PE" => "Pernambuco",
    "PI" => "Piauí",
    "RJ" => "Rio de Janeiro",
    "RN" => "Rio Grande do Norte",
    "RO" => "Rondônia",
    "RS" => "Rio Grande do Sul",
    "RR" => "Roraima",
    "SC" => "Santa Catarina",
    "SE" => "Sergipe",
    "SP" => "São Paulo",
    "TO" => "Tocantins"
);

function dateFormat($d, $tipo = true){
    if(!$d){
        return '';
    }
    if ($tipo) {
        $hora = explode(' ', $d);
        $data = explode('-', $hora[0]);
        return $data[2].'/'.$data[1].'/'.$data[0].' '.$hora[1];
    }
    else{
        $hora = explode(' ', $d);
        $data = explode('/', $hora[0]);
        return $data[2].'-'.$data[1].'-'.$data[0].' '.$hora[1];
    }
    


}
function trataUrl($params = array()){
    $url = (isset($_GET['b'])) ? 'busca/' : '';
    foreach($params as $value){
        $url .= $value.'/';
    }
    return $url;
}

function antiinject($var,$quotes=ENT_NOQUOTES,$keeptags=false) {
    //ENT_QUOTES, ENT_NOQUOTES, ENT_COMPAT;
    
    
    if(!is_array($var)){
        $var = stripslashes($var);
        $var = html_entity_decode($var,$quotes,'utf-8');
        if(!$keeptags) {
            $var = strip_tags($var);
        }
        $var = trim($var);
        //$var = utf8_decode($var);
        /**/
        $var = htmlentities($var,$quotes);
        if($keeptags) {
            $var = str_replace('&lt;','<',$var);
            $var = str_replace('&gt;','>',$var);
        }
        /**/
        $var = addslashes($var);
    } else {
        foreach($var as $k=>$ar){
            $var[$k] = antiinject($ar);
        }
    }
    return $var;
}
function ai($str){
    return antiinject($str);
}




?>