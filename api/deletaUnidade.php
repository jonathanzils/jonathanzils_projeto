<?
require "../uteis.php";




$unidade =  new Unidade();
$result = $unidade->deletaUnidade($_POST['id']);


if($result){
    
    $totalRegistros = $unidade->getUnidade()['totalResult'];
    $result = array(
        "status" => 'success',
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Seu registro foi deletado com sucesso."
    );
    echo json_encode($result);
}else{
    $result = array(
        "status" => 'danger',
        "msg" => "ocorreu um erro"
    );
    echo json_encode($result);

}


?>