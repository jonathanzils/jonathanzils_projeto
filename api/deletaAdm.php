<?
require "../uteis.php";




$Adm =  new Adm();
$result = $Adm->deletaAdm($_POST['id']);


if($result){
    
    $totalRegistros = $Adm->getAdm()['totalResult'];
    $result = array(
        "status" => 'success',
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Seu registro foi deletado com sucesso."
    );
    echo json_encode($result);
}else{
    $result = array(
        "status" => 'danger',
        "msg" => "ocorreu um erro"
    );
    echo json_encode($result);

}


?>