<?
require "../uteis.php";

$conselho = new Conselho();
if($conselho->editConselho($_POST)){
    $result = array(
        "status" => 'success',
        "msg" => "Seu registro foi editado com sucesso."
    );
    echo json_encode($result);
}
else{
    $result = array(
        "status" => 'danger',
        "msg" => "Ocorreu um erro"
    );
    echo json_encode($result);

}
?>