<?
require "../uteis.php";

$bloco = new Bloco();
if($bloco->editBloco($_POST)){
    $result = array(
        "status" => 'success',
        "msg" => "Seu registro foi editado com sucesso."
    );
    echo json_encode($result);
}
else{
    $result = array(
        "status" => 'danger',
        "msg" => "Ocorreu um erro"
    );
    echo json_encode($result);

}
?>