<?
require "../uteis.php";

$Usuario = new Usuario();
$dados = array();
foreach($_POST['g'] as $field=>$value){
    $dados[$field] = $value;
}
if($Usuario->editUsuario($dados)){
    $result = array(
        "status" => 'success',
        "msg" => "Seu registro foi editado com sucesso."
    );
    echo json_encode($result);
}
else{
    $result = array(
        "status" => 'danger',
        "msg" => "Ocorreu um erro"
    );
    echo json_encode($result);

}
?>