<?
require "../uteis.php";

$morador =  new Cadastro();
$result = $morador->deletaMorador($_POST['id']);


if($result){
    
    $totalRegistros = $morador->getMorador()['resultSet'];
    $result = array(
        "status" => 'success',
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Seu registro foi deletado com sucesso."
    );
    echo json_encode($result);
}else{
    $result = array(
        "status" => 'danger',
        "msg" => "ocorreu um erro"
    );
    echo json_encode($result);

}


?>