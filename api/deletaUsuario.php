<?
require "../uteis.php";

$Usuario =  new Usuario();
$result = $Usuario->deletaUsuario($_POST['id']);


if($result){
    
    $totalRegistros = $Usuario->getUsuario()['totalResult'];
    $result = array(
        "status" => 'success',
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Seu registro foi deletado com sucesso."
    );
    echo json_encode($result);
}else{
    $result = array(
        "status" => 'danger',
        "msg" => "ocorreu um erro"
    );
    echo json_encode($result);

}


?>