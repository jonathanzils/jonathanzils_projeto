<?
Class Dao{
    public static $instance;
    public $pagination = 0;
    public $busca = array();


    function __construct()
    {
        
    }

    

    public static function getInstace(){
        if(!isset(self::$instance)){
            self::$instance = new Dao();
        }
        return self::$instance;
    }

    public function insertData($qry){
        try {
            $sql = ConnectDB::getInstance()->prepare($qry);
            return $sql->execute();

        } catch (\Throwable $e) {
            legivel($e);
            echo ' query: '.$qry;
            return false;
        }
    }
    
    public function updateData($qry){
        try {
            $sql = ConnectDB::getInstance()->prepare($qry);
            return $sql->execute();
        } catch (\Throwable $e) {
            legivel($e);
            echo ' query: '.$qry;
            return false;
        }
    }
    public function deleteData($qry){
        try {
            $sql = ConnectDB::getInstance()->prepare($qry);
            return $sql->execute();
        } catch (\Throwable $e) {
            legivel($e);
            echo ' query: '.$qry;
            return false;
        }
    }
    public function listarData($qry, $unic=false){
        try{
            if($this->pagination > 0){

                $sql = ConnectDB::getInstance()->prepare($qry);
                $sql->execute();
                $totalResults = $sql->rowCount();
                $totalPaginas = ceil($totalResults/$this->pagination);
                $qry = $qry.$this->limitPagination($this->pagination);
            }
            $sql = ConnectDB::getInstance()->prepare($qry);
            $sql->execute();
            if(!$unic){
                $resultSet = $sql->fetchAll(PDO::FETCH_ASSOC);
            }else{
                $resultSet = $sql->fetch(PDO::FETCH_ASSOC);
            }

            return array(
                'resultSet' => $resultSet,
                'totalResult' => ($totalResults ? $totalResults : $sql->rowCount()),
                'qtPaginas' => ($totalPaginas ? $totalPaginas : 0)
            );
        }catch(Exception $e){
            legivel($e);
            echo ' query: '.$qry;
            return false;
        }
    }

    function limitPagination($qtRegistros){
        $pageAtual = ($_GET['pagina']) ? $_GET['pagina'] : 1;        
        $inicio = ($qtRegistros * $pageAtual) - $qtRegistros;
           
        
        return " LIMIT ".$inicio.", ".$qtRegistros;
    }
    function renderPagination($qtPaginas){
        global $url_site;
        $pagAtual = ($_GET['pagina']) ? $_GET['pagina'] : 1;
        $url = $url_site.$_GET['page'].'/'.trataUrl($_GET['b']);

        $estrutura = '
        <nav aria-label="Page navigation example">
            <ul class="pagination">';
        if ($pagAtual > 1) {
        $estrutura .= '<li class="page-item"><a class="page-link bg-dark text-white li" href="' .$url. 'pagina/1" aria-label="Previous"><span aria-hidden="true">Primeira</span></a></li>';
        $estrutura .= '<li class="page-item"><a class="page-link bg-dark text-white li" href="' .$url. 'pagina/' . ($pagAtual - 1) . '" aria-label="Previous"><span aria-hidden="true">&laquo;<span></a></li>';
        }
                $decremento = ($pagAtual > 3 ? $pagAtual-3 : 1);
                for ($i=$decremento; $i < $pagAtual; $i++) { 
                    $estrutura .= '<li class="page-item"><a class="page-link bg-dark text-white li" href="'.$url.'pagina/'.$i.'">'.$i.'</a></li>';
                }

                $estrutura .= '<li class="page-item active "><a class="page-link bg-dark text-white" href="'.$url.'pagina/'.$pagAtual.'">'.$pagAtual.'</a></li>';
                if($pagAtual <= $qtPaginas){
                    $calculaIncremento = $qtPaginas-$pagAtual;
                    $incremento = ( $calculaIncremento > 4 ? 3 : $calculaIncremento);
                    for ($i=$pagAtual+1; $i <= $pagAtual+$incremento; $i++) { 
                        $estrutura .= '<li class="page-item"><a class="page-link bg-dark text-white li" href="'.$url.'pagina/'.$i.'">'.$i.'</a></li>';
                    }
                }
                if($pagAtual < $qtPaginas){
                $estrutura .= '<li class="page-item"><a class="page-link bg-dark text-white li" href="'.$url.'pagina/'.($pagAtual + 1).'" aria-label="Next"><span aria-hidden="true">&raquo;<span></a></li>';
                $estrutura .= '
                <li class="page-item"><a class="page-link bg-dark text-white li   " href="'.$url.'pagina/'.$qtPaginas.'" aria-label="Next"><span aria-hidden="true">Última</span></a></li>';}
            $estrutura .= '</ul>
        </nav>';

        return $estrutura;
    }   


}
?>