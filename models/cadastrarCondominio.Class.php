<?
Class Condominio extends Adm{

    function __construct()
    {
        
    }
    function getCondominio($id = null){
        $qry = 'SELECT cond.id, cond.nomeDoCondominio, cond.bloco, cond.logradouro, cond.numero,cond.bairro, cond.cidade, cond.estado, cond.cep, adm.nomeDaAdm AS adm
        FROM jz_condominio cond
        INNER join jz_adm adm ON adm.id = cond.adm ';
          $contaTermos = count($this->busca);

          if ($contaTermos > 0) {
            $i=0;
            
            foreach ($this->busca as $field => $termo) {
              if($i==0 && $termo!=null){
                $qry = $qry.' WHERE ';
                $i++;
              }
              switch (gettype($termo)) {
                case is_numeric($termo):
                    if(!empty($termo)){
                      $qry = $qry.' '.$field.' = '.$termo.' AND ';
      
                    }
                  break;
                  default:
                  if(!empty($termo)){
                    $qry = $qry.' cond.'.$field.' LIKE "%'.$termo.'%"'.' AND ';
      
                  }
                 
                  break;
              }
              
            }
           
            $qry = rtrim($qry, ' AND ');
          }
        if($id){
            $qry .= ' WHERE  cond.id = '.$id;
            $unique = true;
        }
      
        return $this->listarData($qry,$unique);
        
    }
    function setCondominio($dados){
        $values = '';
        $qry = 'INSERT INTO jz_condominio (';
        foreach($dados as $ch=>$value){
            $qry .= '`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $qry = rtrim($qry,', ');
        $qry .=') VALUES ('.rtrim($values,', ').')';
        return $this->insertData($qry);
    }
    function editCondominio($dados){
       
        $qry = 'UPDATE jz_condominio SET';
        foreach($dados as $ch=>$value){
            if($ch != 'edit'){

                $qry .= "`".$ch."`='".$value."', ";
            }
        }
        $qry = rtrim($qry,', ');
        $qry .=" WHERE id=".$dados['edit'];
        return $this->updateData($qry);
    }

    function deletaCondominio($id)
    {
        $qry = 'DELETE FROM jz_condominio WHERE id=' . $id;
        return $this->deleteData($qry);
    }  
    function inputPopulation($id){
        return $_SESSION['condominio'][$id];
    }  
}
?>