<?
Class Unidade extends Bloco{

    function __construct()
    {
        
    }
    function getUnidade($id = null){
        $qry = 'SELECT
        unid.id,
        cond.nomeDoCondominio AS condominio,
        cond.id as idcond,
        bloco.nomeDoBloco AS bloco,
        unid.nomeDaUnidade,
        unid.metragem,
        unid.vagas,
        unid.dataCadastro
        FROM
        jz_unidade unid
        INNER JOIN jz_condominio cond ON cond.id = unid.condominio
        INNER JOIN jz_bloco bloco ON bloco.id = unid.bloco ';
          $contaTermos = count($this->busca);

          if ($contaTermos > 0) {
            $i=0;
            
            foreach ($this->busca as $field => $termo) {
              if($i==0 && $termo!=null){
                $qry = $qry.' WHERE ';
                $i++;
              }
              switch (gettype($termo)) {
                case is_numeric($termo):
                    if(!empty($termo)){
                      $qry = $qry.' '.$field.' = '.$termo.' AND ';
      
                    }
                  break;
                  default:
                  if(!empty($termo)){
                    $qry = $qry.' '.$field.' LIKE "%'.$termo.'%"'.' AND ';
      
                  }
                 
                  break;
              }
              
            }
           
            $qry = rtrim($qry, ' AND ');
          }
        if($id){
            $qry .= ' WHERE unid.id ='.$id;
            $unique = true;
        }
        return $this->listarData($qry, $unique);
        
    }
    function getUnidFromBloco($bloco){
        $qry = 'SELECT id, nomeDaUnidade FROM jz_unidade WHERE bloco='.$bloco;
        return $this->listarData($qry);
    }
    function setUnidade($dados){
        $values = '';
        $qry = 'INSERT INTO jz_unidade (';
        foreach($dados as $ch=>$value){
            $qry .= '`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $qry = rtrim($qry,', ');
        $qry .=') VALUES ('.rtrim($values,', ').')';
        return $this->insertData($qry);
    }
    function editUnidade($dados){
        $qry = 'UPDATE jz_unidade SET';
        foreach($dados as $ch=>$value){
            if($ch != 'edit'){

                $qry .= "`".$ch."`='".$value."', ";
            }
        }
        $qry = rtrim($qry,', ');
        $qry .=" WHERE id=".$dados['edit'];
        return $this->updateData($qry);
    } 

    function deletaUnidade($id){
        $qry = 'DELETE FROM jz_unidade WHERE id=' . $id;
        return $this->deleteData($qry);
   
    }    
    
}
?>