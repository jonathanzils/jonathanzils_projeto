<?
Class Adm extends Dao{


    function __construct()
    {
        
    }
    function getAdm($id = null){
        $qry = 'SELECT * FROM jz_adm';
        $contaTermos = count($this->busca);

        if ($contaTermos > 0) {
          $i=0;
          
          foreach ($this->busca as $field => $termo) {
            if($i==0 && $termo!=null){
              $qry = $qry.' WHERE ';
              $i++;
            }
            switch (gettype($termo)) {
              case is_numeric($termo):
                  if(!empty($termo)){
                    $qry = $qry.' '.$field.' = '.$termo.' AND ';
    
                  }
                break;
                default:
                if(!empty($termo)){
                  $qry = $qry.' '.$field.' LIKE "%'.$termo.'%"'.' AND ';
    
                }
               
                break;
            }
            
          }
         
          $qry = rtrim($qry, ' AND ');
        }
   
        if($id){
            $qry .= ' WHERE id = '.$id;
            $unique = true;
        }
        return $this->listarData($qry,$unique);
    }

    function getLast(){
        $qry = 'SELECT
        nomeDaAdm
        FROM
        jz_adm
        ORDER BY dataCadastro DESC 
        LIMIT 0,5';

        return $this->listarData($qry);
    }
    function setAdm($dados){
        $values = '';
        $qry = 'INSERT INTO jz_adm (';
        foreach($dados as $ch=>$value){
            $qry .= '`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $qry = rtrim($qry,', ');
        $qry .=') VALUES ('.rtrim($values,', ').')';
        return $this->insertData($qry);
    }
    function editAdm($dados){
        $qry = 'UPDATE jz_adm SET';
        foreach($dados as $ch=>$value){
            if($ch != 'edit'){

                $qry .= "`".$ch."`='".$value."', ";
            }
        }
        $qry = rtrim($qry,', ');
        $qry .=" WHERE id=".$dados['edit'];
        return $this->updateData($qry);
    }

    function deletaAdm($id){
        $qry = 'DELETE FROM jz_adm WHERE id=' . $id;
        return $this->deleteData($qry);
    }  
    function inputPopulation($id){
        //return $_SESSION['bloco'][$id];
    }  
}
?>