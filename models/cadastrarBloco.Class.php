<?
Class Bloco extends Condominio{


    function __construct()
    {
        
    }
    function getBloco($id = null){
        $qry = 'SELECT
        bloco.id,
        cond.id as idcond,
        cond.nomeDoCondominio AS condominio,
        bloco.nomeDoBloco,
        bloco.andar,
        bloco.unidades
        FROM
        jz_bloco bloco
        INNER JOIN jz_condominio cond ON cond.id = bloco.condominio ';
           $contaTermos = count($this->busca);

           if ($contaTermos > 0) {
             $i=0;
             
             foreach ($this->busca as $field => $termo) {
               if($i==0 && $termo!=null){
                 $qry = $qry.' WHERE ';
                 $i++;
               }
               switch (gettype($termo)) {
                 case is_numeric($termo):
                     if(!empty($termo)){
                       $qry = $qry.' '.$field.' = '.$termo.' AND ';
       
                     }
                   break;
                   default:
                   if(!empty($termo)){
                     $qry = $qry.' '.$field.' LIKE "%'.$termo.'%"'.' AND ';
       
                   }
                  
                   break;
               }
               
             }
            
             $qry = rtrim($qry, ' AND ');
           }
        if($id){
            $qry .= ' WHERE bloco.id = '.$id;
            $unique = true;
        }
        return $this->listarData($qry,$unique);
    }

    function getBlocoFromCond($cond){
        $qry = 'SELECT id, nomeDoBloco FROM jz_bloco WHERE condominio = '.$cond;
        return $this->listarData($qry);
    }

    function setBloco($dados){
        $values = '';
        $qry = 'INSERT INTO jz_bloco (';
        foreach($dados as $ch=>$value){
            $qry .= '`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $qry = rtrim($qry,', ');
        $qry .=') VALUES ('.rtrim($values,', ').')';
        return $this->insertData($qry);
    }
    function editBloco($dados){
        $qry = 'UPDATE jz_bloco SET';
        foreach($dados as $ch=>$value){
            if($ch != 'edit'){

                $qry .= "`".$ch."`='".$value."', ";
            }
        }
        $qry = rtrim($qry,', ');
        $qry .=" WHERE id=".$dados['edit'];
        return $this->updateData($qry);
    }

    function deletaBloco($id){
        $qry = 'DELETE FROM jz_bloco WHERE id=' . $id;
        return $this->deleteData($qry);
    }  
    
    
}
?>