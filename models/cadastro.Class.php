<?
Class Cadastro extends Unidade{

   function __construct()
   {
       
   }

  function getMorador($id = null)
  {
    $qry = 'SELECT 
      mor.id,
      cond.nomeDoCondominio AS condominio,
      cond.id as idcond,
      bloco.nomeDoBloco AS bloco,
      bloco.id as idbloco,
      unid.nomeDaUnidade AS unidade,
      unid.id as idunid,
      mor.nome,
      mor.cpf,
      mor.email,
      mor.telefone,
      mor.dataCadastro
      FROM
      jz_morador mor 
      INNER JOIN jz_condominio cond ON cond.id = mor.condominio
      INNER JOIN jz_bloco bloco ON bloco.id = mor.bloco
      INNER JOIN jz_unidade unid ON unid.id = mor.unidade';

    $contaTermos = count($this->busca);

    if ($contaTermos > 0) {
      $i=0;
      
      foreach ($this->busca as $field => $termo) {
        if($i==0 && $termo!=null){
          $qry = $qry.' WHERE ';
          $i++;
        }
        switch (gettype($termo)) {
          case is_numeric($termo):
              if(!empty($termo)){
                $qry = $qry.' mor.'.$field.' = '.$termo.' AND ';

              }
            break;
            default:
            if(!empty($termo)){
              $qry = $qry.' mor.'.$field.' LIKE "%'.$termo.'%"'.' AND ';

            }
           
            break;
        }
        
      }
     
      $qry = rtrim($qry, ' AND ');
    }

      if($id){
          $qry .= ' WHERE mor.id = '.$id;
          $unique = true;
      }
      return $this->listarData($qry,$unique);
      
        
   }

   function getAll(){
    $qry = 'SELECT
    count(mor.id) AS morador,
    (SELECT count(id) FROM jz_unidade) AS unidade,
    (SELECT count(id) FROM jz_condominio) AS condominio,
    (SELECT count(id) FROM jz_bloco) AS bloco,
    (SELECT count(id) FROM jz_adm) AS adm
    FROM
    jz_morador mor';

    $unique = true;

    return $this->listarData($qry, $unique);
    
   }

   function getMoradorFromCondominio(){
     $qry = 'SELECT 
     cond.nomeDoCondominio AS condominio,
     count(mor.id) AS morador
     FROM
     jz_morador mor
     LEFT JOIN jz_condominio cond ON cond.id = mor.condominio
     GROUP BY cond.nomeDoCondominio';

     return $this->listarData($qry);
   }

    function setMorador($dados){
      
      $values = '';
      $qry = 'INSERT INTO jz_morador (';
      foreach($dados as $ch=>$value){
          $qry .= '`'.$ch.'`, ';
          $values .= "'".$value."', ";
      }
      $qry = rtrim($qry,', ');
      $qry .=') VALUES ('.rtrim($values,', ').')';
      return $this->insertData($qry);
        
    }

    function editMorador($dados)
    {
      $qry = 'UPDATE jz_morador SET';
        foreach($dados as $ch=>$value){
            if($ch != 'edit'){

                $qry .= "`".$ch."`='".$value."', ";
            }
        }
        $qry = rtrim($qry,', ');
        $qry .=" WHERE id=".$dados['edit'];
        return $this->updateData($qry);
    }

   function deletaMorador($id){
    $qry = 'DELETE FROM jz_morador WHERE id=' . $id;
        return $this->deleteData($qry);
   }
   
//    function inputPopulation($id){
//     return $_SESSION['cadastro'][$id];
// }
}
?>