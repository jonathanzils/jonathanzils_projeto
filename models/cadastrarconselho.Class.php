<?
Class Conselho extends Condominio{


    function __construct()
    {
        
    }
    function getConselho($id = null){
        $qry = 'SELECT
        con.id,
        cond.nomeDoCondominio AS condominio,
        con.nome AS nome,
        con.funcao AS funcao
        FROM
        jz_condominio cond
        INNER JOIN jz_conselho con ON con.condominio = cond.id ';
         $contaTermos = count($this->busca);

         if ($contaTermos > 0) {
           $i=0;
           
           foreach ($this->busca as $field => $termo) {
             if($i==0 && $termo!=null){
               $qry = $qry.' WHERE ';
               $i++;
             }
             switch (gettype($termo)) {
               case is_numeric($termo):
                   if(!empty($termo)){
                     $qry = $qry.' '.$field.' = '.$termo.' AND ';
     
                   }
                 break;
                 default:
                 if(!empty($termo)){
                   $qry = $qry.' '.$field.' LIKE "%'.$termo.'%"'.' AND ';
     
                 }
                
                 break;
             }
             
           }
          
           $qry = rtrim($qry, ' AND ');
         }
        if($id){
            $qry .= ' WHERE con.id = '.$id;
            $unique = true;
        }
        return $this->listarData($qry,$unique);
    }
    function setConselho($dados){
        $values = '';
        $qry = 'INSERT INTO jz_conselho (';
        foreach($dados as $ch=>$value){
            $qry .= '`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $qry = rtrim($qry,', ');
        $qry .=') VALUES ('.rtrim($values,', ').')';
        return $this->insertData($qry);
    }
    function editConselho($dados){
        $qry = 'UPDATE jz_conselho SET';
        foreach($dados as $ch=>$value){
            if($ch != 'edit'){

                $qry .= "`".$ch."`='".$value."', ";
            }
        }
        $qry = rtrim($qry,', ');
        $qry .=" WHERE id=".$dados['edit'];
        return $this->updateData($qry);
    }

    function deletaConselho($id){
        $qry = 'DELETE FROM jz_conselho WHERE id=' . $id;
        return $this->deleteData($qry);
    
    }  
    function inputPopulation($id){
       // return $_SESSION['conselho'][$id];
    }  
}
?>