
<div class="col-12 text-center">
    <h1>Lista de blocos</h1>
</div>

<div class="col-12 col-md-12 filter mt-5">
    <form class="d-flex" id="filtro" method="GET">
        
        <input type="hidden" name="page" value="listaBloco">
        <input class="form-control me-2 termo1" type="search" placeholder="Busca por nome" aria-label="Search"name="b[nomeDoBloco]">
        
        <div class="input-group-prepend col-4">
            <select name="b[condominio]" class="custom-select termo2">
            <option value="">Por Condominio</option>
                <?
                
                foreach($listCondominio['resultSet'] as $condominios){
                    echo '<option value="'.$condominios['id'].'">'.$condominios['nomeDoCondominio'].'</option>';
                }
                ?>
            </select>
        </div>
        <button class="btn btn-outline-primary text-white ml-2" type="submit" disabled>Buscar</button>
        <a href="<?=$url_site?>clientes"class="btn btn-outline-danger text-white ml-2">Limpar</a>
    </form>
</div>

<div class="row">
    <div class="col-12 col-md-12 mt-5">
        <table class="table table-responsive-md table-dark table-striped"id="listaBloco">
            <th>Condominio</th>
            <th>Nome do Bloco</th>
            <th>qtd. de andares</th>
            <th>qtd. de unidades por andar</th>
            <th><a href="index.php?page=cadBlocos" class="btn btn-light"><i class="icofont-ui-add"> Cadastrar</i></a></th>
            <?
            
            foreach($result['resultSet'] as $ch=>$value){?>
            <tr data-id="<?=$value['id']?>">
                <td><?=$value['condominio']?></td>
                <td><?=$value['nomeDoBloco']?></td>
                <td><?=$value['andar']?></td>
                <td><?=$value['unidades']?></td>
                
                <td>
                    <a href="#"name="remove" data-id="<?=$value['id']?>"class="text-white mr-4 removerBloco"><i class="icofont-ui-delete"></i></a>
                    <a href="<?=$url_site?>cadBlocos/<?=$value['id']?>"name="id" class="text-white"><i class="icofont-edit"></i></a> 
                </td>
                
            </tr>  
            <?}?>
            <tr>
                <td colspan="2">&nbsp;</td>
               
                <td colspan="3" class="text-right ">Total de Registros: <small class="badge badge-light totalRegistros"><?=$totalResults?></small></td>

            </tr>
        </table>
        <div class="col-12">
            <?=$pagination?>
        </div>
    </div>
</div>

