
<div class="row">
    <div class="col-12 pb-5">
        <h1 class="col-12 col-sm-12 col-md-12 text-center">Formulario de Condominio</h1>
    </div>
</div>

<form class="form-row formCondominio" action="#" method="POST">
    <div class="col-12 col-md-4">
        <input class="col-12 text-center mb-2" type="text" name="nomeDoCondominio" placeholder="Nome do condominio" value="<?=$popular['nomeDoCondominio']?>"required>
    </div>
    <div class="col-12 col-md-4">
        <input class="col-12 text-center" type="text" name="bloco" placeholder="Quantidade de blocos" value="<?=$popular['bloco']?>">
    </div>
    
    <div class="col-12 col-md-4">
        <input class="col-12 text-center" type="text" name="logradouro"placeholder="Logradouro"value="<?=$popular['logradouro']?>">
    </div>
    <div class="col-12 col-md-1">
        <input class="col-12 text-center" type="text" name="numero"placeholder="N°"value="<?=$popular['numero']?>">
    </div>
    <div class="col-12 col-md-3">
        <input class="col-12 text-center" type="text" name="bairro"placeholder="Bairro"value="<?=$popular['bairro']?>">
    </div>
    <div class="col-12 col-md-4">
        <input class="col-12 text-center" type="text" name="cidade"placeholder="cidade"value="<?=$popular['cidade']?>">
    </div>
    <div class="col-12 col-md-4 mb-2 text-left">
       <select class="col-12 col-md-5 custom-select" name="estado">
           <option>estado</option>
           <?foreach($estados as $ch=>$value){?>
            <option value="<?=$ch?>"<?=($ch == $popular['estado'] ? 'selected' : '')?>><?=$value?></option>
            <?}?>
       </select>
    </div>
    <div class="col-12 col-md-4 mt-2">
        <input class="col-12 text-center" type="text" name="cep"placeholder="CEP" value="<?=$popular['cep']?>">
    </div>
    <select class="col-6 text-center custom-select" name="adm">
        <option >Administradora</option>
        <?foreach($listAdm as $val){?>
        <option value="<?=$val['id']?>" <?=($val['nomeDaAdm'] == $popular['adm'] ? 'selected' : '')?> ><?=$val['nomeDaAdm'];?></option>
        <?}?>
    </select>
    <div class="col-12 col-md-12 text-center">
        <?if($_GET['id']){?>
        <input type="hidden" name="edit" value="<?=$_GET['id']?>">
        <?}?>
        <button type="submit" class="btn bg-blue btn-dark mt-2 px-5 buttonEnviar">enviar</button>
        
    </div>
</form>