
<div class="row">
    <div class="col-12 pb-5">
        <h1 class="col-12 col-sm-12 col-md-12 text-center">Formulario de Cadastro</h1>
    </div>
</div>

<form class="form-row formCliente" action="#" method="POST">
    <div class="col-12 col-md-12 text-center from-group mb-2">
        <select name="condominio" class="col-6 text-center custom-select fromCondominio">
            <option>Condominio</option>
            <? foreach ($listCond as $ch => $val) { ?>
                <option value="<?= $val['id'] ?>" <?= ($val['nomeDoCondominio'] == $popular['condominio'] ? 'selected' : '') ?>><?= $val['nomeDoCondominio'] ?></option>
            <? } ?>
        </select>
    </div>

    <div class="col-12 col-md-12 text-center from-group mb-2">
        <select name="bloco" class="fromBloco col-12 text-center custom-select">
            <?
            if ($_GET['id']) {
                $blocos = $cliente->getBlocoFromCond($popular['idcond']);
                foreach ($blocos['resultSet'] as $bloco) {
            ?>
                    <option value="<?=$bloco['id'] ?>" <?=($bloco['id'] == $popular['bloco'] ? 'selected' : '') ?>><?=$bloco['nomeDoBloco']?></option>
            <?
                }
            }
            ?>
        </select>
    </div>

    <div class="col-12 col-md-12 text-center from-group mb-2">
        <select name="unidade" class="fromUnidade col-12 text-center custom-select">
        <?
            if ($_GET['id']) {
                $unid = $cliente->getUnidFromBloco($popular['idbloco']);
                foreach ($unid['resultSet'] as $unids) {
            ?>
                    <option value="<?=$unids['id'] ?>" <?=($unids['id'] == $popular['unidade'] ? 'selected' : '') ?>><?=$unids['nomeDaUnidade']?></option>
            <?
                }
            }
            ?>

        </select>
    </div>

    <div class="col-12 col-md-6">
        <input class="col-12 text-center mb-2" type="text" name="nome" placeholder="Nome" value="<?= $popular['nome'] ?>" required>
    </div>
    <div class="col-12 col-md-6">
        <input class="col-12 text-center" type="text" name="cpf" placeholder="cpf" value="<?= $popular['cpf'] ?>" required>
    </div>
    <div class="col-12 col-md-6">
        <input class="col-12 text-center" type="text" name="email" placeholder="E-mail" value="<?= $popular['email'] ?>" required>
    </div>
    <div class="col-12 col-md-6">
        <input class="col-12 text-center" type="text" name="telefone" placeholder="Telefone" value="<?= $popular['telefone'] ?>">
    </div>
    <div class="col-12 col-md-12 text-center">
        <? if ($_GET['id']) { ?>
            <input type="hidden" name="edit" value="<?= $_GET['id'] ?>">
        <? } ?>
        <button type="submit" class="btn bg-blue btn-dark mt-2 px-5 buttonEnviar">enviar</button>

    </div>
</form>