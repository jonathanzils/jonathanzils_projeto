
<div class="col-12 text-center">
    <h1>Lista de Usuarios</h1>
</div>
<div class="row">
    <div class="col-12 col-md-12 mt-5">
        <table class="table table-responsive-md table-dark table-striped"id="listaUsuario">
            <th>Nome do usuario</th>
            <th>usuario</th>
            <th><a href="index.php?page=cadUsuario" class="btn btn-light"><i class="icofont-ui-add"> Cadastrar</i></a></th>
            <?
           
            foreach($result['resultSet'] as $ch=>$value){
                ?>
            <tr data-id="<?=$value['id']?>">
                <td><?=$value['nome']?></td>
                <td><?=$value['usuario']?></td>
                
                <td>
                    <a href="#<?=$ch?>"name="remove" data-id="<?=$value['id']?>"class="text-white mr-4 removerUsuario"><i class="icofont-ui-delete"></i></a>
                    <a href="<?=$url_site?>cadUsuario/<?=$value['id']?>"name="id" class="text-white"><i class="icofont-edit"></i></a> 
                </td>
                
            </tr>  
            <?}?>
            <tr>
                <td colspan="">&nbsp;</td>
               
                <td colspan="2" class="text-right ">Total de Registros: <small class="badge badge-light totalRegistros"><?=$totalResults?></small></td>

            </tr>
        </table>
        <div class="col-12">
            <?=$pagination?>
        </div>
    </div>
</div>

