<div class="col-12 text-center">
    <h1>Lista de pessoas</h1>
</div>
<div class="col-12 col-md-12 filter mt-5">
    <form class="d-flex" id="filtro" method="GET">
        
        <input type="hidden" name="page" value="clientes">
        <input class="form-control me-2 termo1" type="search" placeholder="Busca por nome" aria-label="Search"name="b[nome]">
        
        <div class="input-group-prepend col-4">
            <select name="b[condominio]" class="custom-select termo2">
            <option value="">Por Condominio</option>
                <?
                
                foreach($listCondominio['resultSet'] as $condominios){
                    echo '<option value="'.$condominios['id'].'">'.$condominios['nomeDoCondominio'].'</option>';
                }
                ?>
            </select>
        </div>
        <button class="btn btn-outline-primary text-white ml-2" type="submit" disabled>Buscar</button>
        <a href="<?=$url_site?>clientes"class="btn btn-outline-danger text-white ml-2">Limpar</a>
    </form>
</div>
<div class="row">
    <div class="col-12 col-md-12 mt-5">
        <table class="table table-responsive-md table-dark table-striped"id="listaClientes">
            <th>Condominio</th>
            <th>Bloco</th>
            <th>Unidade</th>
            <th>Nome</th>
            <th>CPF</th>
            <th>E-mail</th>
            <th>Telefone</th>
            <th>DT. Cadastro/Ultima atualização</th>
           
            <th><a href="index.php?page=cadastro" class="btn btn-light"><i class="icofont-ui-add"> Cadastrar</i></a></th>
            <?
           
            foreach($result['resultSet'] as $ch=>$value){?>
            <tr data-id="<?=$value['id']?>">
                <td><?=$value['condominio']?></td>
                <td><?=$value['bloco']?></td>
                <td><?=$value['unidade']?></td>
                <td><?=$value['nome']?></td>
                <td><?=$value['cpf']?></td>
                <td><?=$value['email']?></td>
                <td><?=($value['telefone']) ? $value['telefone'] : 'N/A'?></td>
                <td><?=$value['dataCadastro']?></td>
                
                <td>
                    <a href="#"name="remove" data-id="<?=$value['id']?>"class="text-white mr-4 removerCliente"><i class="icofont-ui-delete"></i></a>
                    <a href="<?=$url_site?>cadastro/<?=$value['id']?>"name="id" class="text-white"><i class="icofont-edit"></i></a> 
                </td>
                
            </tr>  
            <?}?>
            <tr>
                <td colspan="4">&nbsp;</td>
               
                <td colspan="6" class="text-right ">Total de Registros: <small class="badge badge-light totalRegistros"><?=$totalRegistros?></small></td>

            </tr>
        </table>
        <div class="col-12">
            <?=$pagination?>
        </div>
    </div>
</div>

