<div class="col-12 text-center">
    <h1>Lista de condominios</h1>
</div>

<div class="col-12 col-md-12 filter mt-5">
    <form class="d-flex" id="filtro" method="GET">
        
        <input type="hidden" name="page" value="listacondominios">
        <input class="form-control me-2 termo1" type="search" placeholder="Busca por nome" aria-label="Search"name="b[nomeDoCondominio]">
        
        <div class="input-group-prepend col-4">
            <select name="b[adm.nomeDaAdm]" class="custom-select termo2">
            <option value="">Por ADM</option>
                <?
                
                foreach($listaAdm['resultSet'] as $adms){
                    echo '<option value="'.$adms['id'].'">'.$adms['nomeDaAdm'].'</option>';
                }
                ?>
            </select>
        </div>
        <button class="btn btn-outline-primary text-white ml-2" type="submit" disabled>Buscar</button>
        <a href="<?=$url_site?>listaCondominios"class="btn btn-outline-danger text-white ml-2">Limpar</a>
    </form>
</div>

<div class="row">
    <div class="col-12 col-md-12 mt-5">
        <table class="table table-responsive-md table-dark table-striped"id="listaCondominios">
            <th>Condominio</th>
            <th>Qtd. de blocos</th>
            <th>Logradouro</th>
            <th>N°</th>
            <th>Bairro</th>
            <th>Cidade</th>
            <th>estado</th>
            <th>CEP</th>
            <th>Adiministradora</th>
            <th><a href="<?=$url_site?>cadCondominio" class="btn btn-light"><i class="icofont-ui-add"> Cadastrar</i></a></th>
            <?
      
            foreach($result['resultSet'] as $ch=>$value){
                ?>
            <tr data-id="<?=$value['id']?>">
                <td><?=$value['nomeDoCondominio']?></td>
                <td><?=$value['bloco']?></td>
                <td><?=$value['logradouro']?></td>
                <td><?=$value['numero']?></td>
                <td><?=$value['bairro']?></td>
                <td><?=$value['cidade']?></td>    
                <td><?=$value['estado']?></td>  
                <td><?=$value['cep']?></td>  
                <td><?=$value['adm']?></td>        
                <td>
                    <a href="#"name="remove" data-id="<?=$value['id']?>"class="text-white mr-4 removerCondominio"><i class="icofont-ui-delete"></i></a>
                    <a href="<?=$url_site?>cadCondominio/<?=$value['id']?>"name="id" class="text-white"><i class="icofont-edit"></i></a> 
                </td>
                
            </tr>  
            <?}?>
            <tr>
                <td colspan="4">&nbsp;</td>
               
                <td colspan="6" class="text-right ">Total de Registros: <small class="badge badge-light totalRegistros"><?=$totalRegistros?> </small></td>

            </tr>
        </table>
        <div class="col-12">
            <?=$pagination?>
        </div>
    </div>
</div>
