<?

?>
<div class="row">
   
    <div class="card text-center tam bg-l col-12 col-md-2 mr-4 " style="width: 18rem;">
        <h2><?=$listAll['resultSet']['condominio']?></h2>
        <div class="card-body bg-blue">
            <p class="card-text">Condominio</p>
        </div>
    </div>
    <div class="card text-center tam bg-l col-12 col-md-2 mr-4" style="width: 18rem;">
        <h2><?=$listAll['resultSet']['unidade']?></h2>
        <div class="card-body bg-blue">
            <p class="card-text">Unidades</p>
        </div>
    </div>
    
    <div class="card text-center tam bg-l col-12 col-md-2 mr-4" style="width: 18rem;">
        <h2><?=$listAll['resultSet']['bloco']?></h2>
        <div class="card-body bg-blue">
            <p class="card-text">Blocos</p>
        </div>
    </div>
    <div class="card text-center tam bg-l col-12 col-md-2 mr-4" style="width: 18rem;">
        <h2><?=$listAll['resultSet']['adm']?></h2>
        <div class="card-body bg-blue">
            <p class="card-text">Administradoras</p>
        </div>
    </div>
    <div class="card text-center tam bg-l col-12 col-md-2" style="width: 18rem;">
        <h2><?=$listAll['resultSet']['morador']?></h2>
        <div class="card-body bg-blue">
            <p class="card-text">Moradores</p>
        </div>
    </div>
  
</div>
<div class="row">
    <div class="col-12 col-md-4 mt-5">
        <h3 class="text-center text-white mb-3">Condominios</h3>
        <table class="table table-dark table-striped text-center"id="listaAdm">
            <th>Condominio</th>
            <th>Numero de moradores</th>
            <?        
            foreach($listMorCond['resultSet'] as $ch=>$value){?>
            <tr data-id="<?=$value['id']?>">
                <td><?=$value['condominio']?></td>
                <td><?=$value['morador']?></td>      
            </tr>
            <?}?>
        </table>
    </div>
    <div class="col-12 col-md-7 mt-5">
    <h3 class="text-center text-white mb-3">Ultimas 5 Administradoras</h3>
        <table class="table table-dark table-striped text-center"id="listaAdm">
            <?        
            foreach($listLastAdm['resultSet'] as $ch=>$value){?>
            <tr data-id="<?=$value['id']?>">
                <td><?=$value['nomeDaAdm']?></td>
            </tr>
            <?}?>
        </table>
    </div>
</div>