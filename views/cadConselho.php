
<div class="row">
    <div class="col-12 pb-5">
        <h1 class="col-12 col-sm-12 col-md-12 text-center">Formulario de cadastro</h1>
    </div>
</div>

<form class="form-row formConselho" action="#" method="POST">

    <div class="col-12 col-md-12 text-center from-group">
        <select name="condominio" class="col-6 text-center mb-2 custom-select">
        <option>condominio</option>
           <?foreach($listCond as $value){?>
            <option value="<?=$value['id']?>"<?=($value['nomeDoCondominio'] == $popular['condominio'] ? 'selected' : '')?>><?=$value['nomeDoCondominio']?></option>
            <?}?>
        </select>

    </div> 

    <div class="col-12 col-md-12 text-center from-group mt-2">
        <input class="col-6 text-center mb-2" type="text" name="nome" placeholder="nome" value="<?=$popular['nome']?>"required>
    </div>
    <div class="col-12 col-md-12 text-center from-group">
    <select name="funcao" class="col-6 text-center mb-2 custom-select">
        <option>função</option>
            <option value="1">Sindico</option>
            <option value="2">Sub sindico</option>
            <option value="3">Conselheiro</option>
        </select>
    </div>
    
    <div class="col-12 col-md-12 text-center">
        <?if($_GET['id']){?>
        <input type="hidden" name="edit" value="<?=$_GET['id']?>">
        <?}?>
        <button type="submit" class="btn bg-blue btn-dark mt-2 px-5 buttonEnviar">enviar</button>
        
    </div>
</form>