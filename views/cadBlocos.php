
<div class="row">
    <div class="col-12 pb-5">
        <h1 class="col-12 col-sm-12 col-md-12 text-center">Formulario de cadastro</h1>
    </div>
</div>

<form class="form-row formBloco" action="#" method="POST">
    <div class="col-12 col-md-12 text-center from-group">
    <select class="col-6 text-center custom-select" name="condominio">
           <option>condominio</option>
           <?foreach($listCond as $value){?>
            <option value="<?=$value['id']?>" <?=($val['nomeDoCondominio'] == $popular['idcond'] ? 'selected' : '')?>><?=$value['nomeDoCondominio']?></option>
            <?}?>
       </select>
    </div>
    <div class="col-12 col-md-12 text-center from-group mt-2">
        <input class="col-6 text-center mb-2" type="text" name="nomeDoBloco" placeholder="Nome do bloco" value="<?=$popular['nomeDoBloco']?>"required>
    </div>
    <div class="col-12 col-md-12 text-center from-group">
        <input class="col-6 text-center" type="text" name="andar" placeholder="Quantos andares?" value="<?=$popular['andar']?>"required>
    </div>
    <div class="col-12 col-md-12 text-center from-group mt-2">
        <input class="col-6 text-center" type="text" name="unidades"placeholder="Quantidade de unidades por andar" value="<?=$popular['unidades']?>"required>
    </div>
    
    <div class="col-12 col-md-12 text-center">
        <?if($_GET['id']){?>
        <input type="hidden" name="edit" value="<?=$_GET['id']?>">
        <?}?>
        <button type="submit" class="btn bg-blue btn-dark mt-2 px-5 buttonEnviar">enviar</button>
        
    </div>
</form>
