
<div class="col-12 text-center">
    <h1>Lista de Administradoras</h1>
</div>

<div class="col-12 col-md-12 filter mt-5">
    <form class="d-flex" id="filtro" method="GET">
        
        <input type="hidden" name="page" value="listaAdm">
        <input class="form-control me-2 termo1" type="search" placeholder="Busca por nome" aria-label="Search"name="b[nomeDaAdm]">
        
       
        <button class="btn btn-outline-primary text-white ml-2" type="submit" disabled>Buscar</button>
        <a href="<?=$url_site?>listaAdm"class="btn btn-outline-danger text-white ml-2">Limpar</a>
    </form>
</div>


<div class="row">
    <div class="col-12 col-md-12 mt-5">
        <table class="table table-responsive-md table-dark table-striped"id="listaAdm">
            <th>Administradora</th>
            <th>CNPJ</th>
            <th>Dt. cadastro/atualização</th>
            <th><a href="index.php?page=cadAdm" class="btn btn-light"><i class="icofont-ui-add"> Cadastrar</i></a></th>
            <?
           
            foreach($result['resultSet'] as $ch=>$value){?>
            <tr data-id="<?=$value['id']?>">
                <td><?=$value['nomeDaAdm']?></td>
                <td><?=$value['cnpj']?></td>
                <td><?=$value['dataCadastro']?></td>
               
                
                <td>
                    <a href="#"name="remove" data-id="<?=$value['id']?>"class="text-white mr-4 removerAdm"><i class="icofont-ui-delete"></i></a>
                    <a href="<?=$url_site?>cadAdm/<?=$value['id']?>"name="id" class="text-white"><i class="icofont-edit"></i></a> 
                </td>
                
            </tr>  
            <?}?>
            <tr>
                <td colspan="2">&nbsp;</td>
               
                <td colspan="3" class="text-right ">Total de Registros: <small class="badge badge-light totalRegistros"><?=$totalResults?></small></td>

            </tr>
        </table>
    </div>
</div>
