
<div class="row">
    <div class="col-12 pb-5">
        <h1 class="col-12 col-sm-12 col-md-12 text-center">Formulario de cadastro</h1>
    </div>
</div>

<form class="form-row formUnidade" action="#" method="POST">

    <div class="col-12 col-md-12 text-center from-group mt-2 mb-2">
        <select name="condominio"class="fromCondominio col-6 text-center custom-select">
            <option>Condominio</option>
            <?foreach($listCond as $ch=>$val){?>
            <option value="<?=$val['id']?>"<?=($val['nomeDoCondominio'] == $popular['condominio'] ? 'selected' : '')?>><?=$val['nomeDoCondominio']?></option>
            <?}?>
        </select>
    </div>

    <div class="col-12 col-md-12 text-center from-group mb-2">
        <select name="bloco"class="fromBloco col-6 text-center custom-select">
        <?
            if ($_GET['id']) {
                $blocos = $unidade->getBlocoFromCond($popular['idcond']);
                foreach ($blocos['resultSet'] as $bloco) {
            ?>
                    <option value="<?=$bloco['id'] ?>" <?=($bloco['id'] == $popular['bloco'] ? 'selected' : '') ?>><?=$bloco['nomeDoBloco']?></option>
            <?
                }
            }
            ?>
        </select>
    </div>

    <div class="col-12 col-md-12 text-center from-group">
        <input class="col-6 text-center mb-2" type="text" name="nomeDaUnidade" placeholder="Nome do unidade" value="<?=$popular['nomeDaUnidade']?>"required>
    </div>
    <div class="col-12 col-md-12 text-center from-group">
        <input class="col-6 text-center" type="text" name="metragem" placeholder="Metragem da unidade" value="<?=$popular['metragem']?>"required>
    </div>
    <div class="col-12 col-md-12 text-center from-group mt-2">
        <input class="col-6 text-center" type="text" name="vagas"placeholder="Qtd. de vagas de garagem" value="<?=$popular['vagas']?>"required>
    </div>
    
    <div class="col-12 col-md-12 text-center">
        <?if($_GET['id']){?>
        <input type="hidden" name="edit" value="<?=$_GET['id']?>">
        <?}?>
        <button type="submit" class="btn bg-blue btn-dark mt-2 px-5 buttonEnviar">enviar</button>
        
    </div>
</form>
