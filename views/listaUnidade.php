
<div class="col-12 text-center">
    <h1>Lista de Unidades</h1>
</div>

<div class="col-12 col-md-12 filter mt-5">
    <form class="d-flex" id="filtro" method="GET">
        
        <input type="hidden" name="page" value="listaUnidade">
        <input class="form-control me-2 termo2" type="search" placeholder="Busca por nome" aria-label="Search"name="b[nomeDaUnidade]">
        
        <div class="input-group-prepend col-4">
            <select name="b[cond.nomeDoCondominio]" class="custom-select termo1">
            <option value="">Por Condominio</option>
                <?
                
                foreach($listCondominio['resultSet'] as $condominios){
                    echo '<option value="'.$condominios['nomeDoCondominio'].'">'.$condominios['nomeDoCondominio'].'</option>';
                }
                ?>
            </select>
        </div>
        <button class="btn btn-outline-primary text-white ml-2" type="submit" disabled>Buscar</button>
        <a href="<?=$url_site?>listaUnidade"class="btn btn-outline-danger text-white ml-2">Limpar</a>
    </form>
</div>


<div class="row">
    <div class="col-12 col-md-12 mt-5">
        <table class="table table-responsive-md table-dark table-striped"id="listaUnidade">
            <th>Condominio</th>
            <th>Bloco</th>
            <th>Nome da Unidade</th>
            <th>Metragem da unidade</th>
            <th>qtd. vagas de garagem</th>
            <th><a href="index.php?page=cadUnidade" class="btn btn-light"><i class="icofont-ui-add"> Cadastrar</i></a></th>
            <?
           
            foreach($result['resultSet'] as $ch=>$value){
                ?>
            <tr data-id="<?=$value['id']?>">
                <td><?=$value['condominio']?></td>
                <td><?=$value['bloco']?></td>
                <td><?=$value['nomeDaUnidade']?></td>
                <td><?=$value['metragem']?></td>
                <td><?=$value['vagas']?></td>
                
                <td>
                    <a href="#<?=$ch?>"name="remove" data-id="<?=$value['id']?>"class="text-white mr-4 removerUnidade"><i class="icofont-ui-delete"></i></a>
                    <a href="<?=$url_site?>cadUnidade/<?=$value['id']?>"name="id" class="text-white"><i class="icofont-edit"></i></a> 
                </td>
                
            </tr>  
            <?}?>
            <tr>
                <td colspan="2">&nbsp;</td>
               
                <td colspan="4" class="text-right ">Total de Registros: <small class="badge badge-light totalRegistros"><?=$totalResults?></small></td>

            </tr>
        </table>
        <div class="col-12">
            <?=$pagination?>
        </div>
    </div>
</div>

