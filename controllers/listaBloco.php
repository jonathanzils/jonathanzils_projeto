<?
$bloco = new Bloco();
$bloco->pagination = 4;


$condominio = new Condominio();
$listCondominio = $condominio->getCondominio();

if(isset($_GET['b'])){
    $montaBusca = array();
    foreach($_GET['b'] as $field=>$termo){
        switch ($field) {
            case 'termo1':
                $montaBusca['nomeDoBloco'] = $termo;
                break;
            case 'termo2':
                $montaBusca['condominio'] = $termo;
                break;
            default:
                # code...
                break;
        }
    }
}


$bloco->busca = $montaBusca;
$result = $bloco->getBloco();

$totalResults = ($result['totalResult'] < 10) ? '0'.$result['totalResult'] : $result['totalResult'];
$pagination = $bloco->renderPagination($result['qtPaginas']);

?>