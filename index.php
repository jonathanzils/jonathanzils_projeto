<?
include('uteis.php');
$user = new Restrito();
if(!$user->acesso()){
    header("Location : login.php");
}
if($_GET['page'] == 'logout'){
    if($user->logout()){
        header('Location: '.$url_site.'login.php');
    }
}
//session_destroy();
?>
<!DOCTYPE html>
<html lang="pt-BR" class="bg-dark">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?=$url_site?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=$url_site?>css/app.css">
    <link rel="stylesheet" href="<?=$url_site?>icofont/icofont.min.css">
    <title>projeto</title>
</head>

<body class="bg-dark">

    <nav class="navbar navbar-expand-lg navbar-dark bg-blue">
        <a class="navbar-brand" href="#"><i class="icofont-cubes icofont-3x"></i></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <? foreach ($nav as $ch => $value) {
                    if (is_array($value)) { ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?= $ch ?>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <? foreach ($value as $ch => $submenu) { ?>
                                    <a class="dropdown-item" href="<?=$url_site.$ch ?>"><?= $submenu ?></a>
                                <? } ?>
                            </div>
                        </li>
                    <? } else { ?>
                        <li class="nav-item active">
                            <a class="nav-link" href="<?=$url_site.$ch?>"><?= $value ?></a>
                        </li>
                    <? } ?>
                <? } ?>

            </ul>
            <div class="ml-auto">
                <?
                $nome = explode(' ', $_SESSION['USUARIO']['nome']);
                ?>
                <small class="text-white">Olá <?= $nome[0]; ?>, seja bem vindo!</small>
            </div>
            <div class="ml-auto">
                <ul class="nav-bar nav">
                    <li class="nav-item config"><a href="<?=$url_site?>logout" class="nav-link text-white"><i class="icofont-logout icofont-2x"></i></a></li>
                </ul>
            </div>
        </div>
    </nav>
    <main class="container pt-5 bg-dark">
        <?
        switch ($_GET['page']) {
            case '':
            case 'inicio':
                require "controllers/inicio.php";
                require "views/inicio.php";
            default:
                require 'controllers/'.$_GET['page'] . '.php';
                require 'views/'.$_GET['page'] . '.php';
                break;
        }
        ?>
        
    </main>
    <footer class="bg-blue text-center text-white">&copy;Todos os direitos reservados</footer>


    <script>var url_site = '<?=$url_site?>';</script>
    <script src="<?=$url_site?>js/jquery-3.6.0.min.js"></script>
    <script src="<?=$url_site?>js/jquery.mask.min.js"></script>
    <script src="<?=$url_site?>js/bootstrap.bundle.min.js"></script>
    <script src="<?=$url_site?>js/app.js"></script>


</body>

</html>