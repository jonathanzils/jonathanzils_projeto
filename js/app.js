$(function(){
//cliente
    $('.formCliente').submit(function(){
        var edit = $(this).find('input[name="edit"]').val()
        var url;
        if(edit){
            url = url_site+"api/editaMorador.php";
            urlRedir = url_site+"clientes";
        }
        else{
            url = url_site+"api/cadastraMorador.php";
            urlRedir = url_site+"cadastro";
        }
        
        $('.buttonEnviar').attr('disabled', 'disable');  

    $.ajax({
        url: url,
        dataType: "json",
        type: "POST",
        data: $(this).serialize(),
        
        success : function(data){
            if(data.status == 'success'){
               
                myAlert(data.status, data.msg, 'main', urlRedir);
            }
            else{
                myAlert(data.status, data.msg, 'main', urlRedir);
            }
        }
    });
    return false;
    })

    $('#listaClientes').on('click','.removerCliente', function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deletaMorador.php',
            dataType: 'json',
            type:'POST',
            data:{ id: idRegistro},
            success : function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', url_site+'clientes');
                }
                else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        });

        return false;
    });
    //condominio
    $('.formCondominio').submit(function(){
        var edit = $(this).find('input[name="edit"]').val()
        var url;
        if(edit){
            url = url_site+"api/editaCondominio.php";
            urlRedir = url_site+"listaCondominios";
        }
        else{
            url = url_site+"api/cadastraCondominio.php";
            urlRedir = url_site+"cadCondominio";
        }
        
        $('.buttonEnviar').attr('disabled', 'disable');  

    $.ajax({
        url: url,
        dataType: "json",
        type: "POST",
        data: $(this).serialize(),
        success : function(data){
            if(data.status == 'success'){
                myAlert(data.status, data.msg, 'main', urlRedir);
            }
            else{
                myAlert(data.status, data.msg, 'main', urlRedir);
            }
        }
    });
    return false;
    })
    $('#listaCondominios').on('click','.removerCondominio', function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deletaCondominio.php',
            dataType: 'json',
            type:'POST',
            data:{ id: idRegistro},
            success : function(data){
                if(data.status == 'success'){
                   
                    myAlert(data.status, data.msg, 'main', url_site+'listaCondominio');
                }
                else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        });

        return false;
    });
    //bloco
    $('.formBloco').submit(function(){
        var edit = $(this).find('input[name="edit"]').val()
        var url;
        if(edit){
            url = url_site+"api/editaBloco.php";
            urlRedir = url_site+"listaBloco";
        }
        else{
            url = url_site+"api/cadastraBloco.php";
            urlRedir = url_site+"cadBlocos";
        }
        
        $('.buttonEnviar').attr('disabled', 'disable');  

    $.ajax({
        url: url,
        dataType: "json",
        type: "POST",
        data: $(this).serialize(),
        success : function(data){
            if(data.status == 'success'){
                myAlert(data.status, data.msg, 'main', urlRedir);
            }
            else{
                myAlert(data.status, data.msg, 'main', urlRedir);
            }
        }
    });
    return false;
    })
    $('#listaBloco').on('click','.removerBloco', function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deletaBloco.php',
            dataType: 'json',
            type:'POST',
            data:{ id: idRegistro},
            success : function(data){
                if(data.status == 'success'){
                   
                    myAlert(data.status, data.msg, 'main', url_site+'listaBloco');
                }
                else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        });

        return false;
    });
//Unidade
$('.formUnidade').submit(function(){
    var edit = $(this).find('input[name="edit"]').val()
    var url;
    if(edit){
        url = url_site+"api/editaUnidade.php";
        urlRedir = url_site+"listaUnidade";
    }
    else{
        url = url_site+"api/cadastraUnidade.php";
        urlRedir = url_site+"cadUnidade";
    }
    
    $('.buttonEnviar').attr('disabled', 'disable');  

$.ajax({
    url: url,
    dataType: "json",
    type: "POST",
    data: $(this).serialize(),
    success : function(data){
        if(data.status == 'success'){
            myAlert(data.status, data.msg, 'main', urlRedir);
        }
        else{
            myAlert(data.status, data.msg, 'main', urlRedir);
        }
    }
});
return false;
})
$('#listaUnidade').on('click','.removerUnidade', function(){
    var idRegistro = $(this).attr('data-id');
    $.ajax({
        url: url_site+'api/deletaUnidade.php',
        dataType: 'json',
        type:'POST',
        data:{ id: idRegistro},
        success : function(data){
            if(data.status == 'success'){
                
                myAlert(data.status, data.msg, 'main',  url_site+'listaUnidade');
            }
            else{
                myAlert(data.status, data.msg, 'main');
            }
        }
    });

    return false;
});
//conselho
$('.formConselho').submit(function(){
    var edit = $(this).find('input[name="edit"]').val()
    var url;
    if(edit){
        url = url_site+"api/editaConselho.php";
        urlRedir = url_site+"listaConselho";
    }
    else{
        url = url_site+"api/cadastraConselho.php";
        urlRedir = url_site+"cadConselho";
    }
    
    $('.buttonEnviar').attr('disabled', 'disable');  

$.ajax({
    url: url,
    dataType: "json",
    type: "POST",
    data: $(this).serialize(),
    success : function(data){
        if(data.status == 'success'){
            myAlert(data.status, data.msg, 'main', urlRedir);
        }
        else{
            myAlert(data.status, data.msg, 'main', urlRedir);
        }
    }
});
return false;
})
$('#listaConselho').on('click','.removerConselho', function(){
    var idRegistro = $(this).attr('data-id');
    $.ajax({
        url: url_site+'api/deletaConselho.php',
        dataType: 'json',
        type:'POST',
        data:{ id: idRegistro},
        success : function(data){
            if(data.status == 'success'){
                
                myAlert(data.status, data.msg, 'main',  url_site+'listaConselho');
            }
            else{
                myAlert(data.status, data.msg, 'main');
            }
        }
    });

    return false;
});
//adm
$('.formAdm').submit(function(){
    var edit = $(this).find('input[name="edit"]').val()
    var url;
    if(edit){
        url = url_site+"api/editaAdm.php";
        urlRedir = url_site+"listaAdm";
    }
    else{
        url = url_site+"api/cadastraAdm.php";
        urlRedir = url_site+"cadAdm";
    }
    
    $('.buttonEnviar').attr('disabled', 'disable');  

$.ajax({
    url: url,
    dataType: "json",
    type: "POST",
    data: $(this).serialize(),
    success : function(data){
        if(data.status == 'success'){
            myAlert(data.status, data.msg, 'main', urlRedir);
        }
        else{
            myAlert(data.status, data.msg, 'main', urlRedir);
        }
    }
});
return false;
})
$('#listaAdm').on('click','.removerAdm', function(){
    var idRegistro = $(this).attr('data-id');
    $.ajax({
        url: url_site+'api/deletaAdm.php',
        dataType: 'json',
        type:'POST',
        data:{ id: idRegistro},
        success : function(data){
            if(data.status == 'success'){
                
                myAlert(data.status, data.msg, 'main',  url_site+'listaAdm');
            }
            else{
                myAlert(data.status, data.msg, 'main');
            }
        }
    });

    return false;
});
//getCond
$('.fromCondominio').change(function(){
        selecionado = $(this).val();

        $.ajax({
            url: url_site+'api/listBlocos.php',
            dataType: 'json',
            type: 'POST',
            data: { id:selecionado },
            success : function(data){
                selectPopulation('.fromBloco',data.resultSet,'nomeDoBloco');
            }    
        })
    })
    //getBloc
    $('.fromBloco').change(function(){
        selecionado = $(this).val();
        
        $.ajax({
            url: url_site+'api/listUnidade.php',
            dataType: 'json',
            type: 'POST',
            data: { id:selecionado },
            success : function(data){
                selectPopulation('.fromUnidade',data.resultSet,'nomeDaUnidade');
            }    
        })
    })

    function selectPopulation(seletor, dados, field){
        
        estrutura = '<option value="">selecione...</option>';
        for (let i = 0; i < dados.length; i++) {
    
            estrutura += '<option value="'+dados[i].id+'">'+dados[i][field]+'</option>'
    
        }
    
        $(seletor).html(estrutura);        
    }

    //usuario

    $('.formUsuario').submit(function(){
        var senha = $(this).find('#senha').val()
        var confSenha = $(this).find('#confSenha').val()
        var url = url_site+"api/cadastraUsuario.php";
        var edit = $(this).find('input[name="g[edit]').val()
        if(edit){
            url = url_site+"api/editaUsuario.php";
            urlRedir = url_site+"listaUsuario";
        }
        else{
            url = url_site+"api/cadastraUsuario.php";
            urlRedir = url_site+"cadUsuario";
        }
   
        $('.buttonEnviar').attr('disabled', 'disable');  
        if(senha == confSenha){
            console.log(url);
            $.ajax({
                url: url,
                dataType: "json",
                type: "POST",
                data: $(this).serialize(),
                
                success : function(data){
                    if(data.status == 'success'){
                       
                        myAlert(data.status, data.msg, 'main', urlRedir);
                    }
                    else{
                        myAlert(data.status, data.msg, 'main', urlRedir);
                    }
                }
            })

        }else{
            myAlert('danger', 'senha não confere', 'main', 'index.php?page=cadUsuario')
        }
    return false;
    })

    $('#listaUsuario').on('click','.removerUsuario', function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deletaUsuario.php',
            dataType: 'json',
            type:'POST',
            data:{ id: idRegistro},
            success : function(data){
                if(data.status == 'success'){
                    $('tr[data-id="'+idRegistro+'"]').remove();
                    $('.totalRegistros').html(data.totalRegistros);
                    myAlert(data.status, data.msg, 'main');
                }
                else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        });

        return false;
    });
    //controlador filtro
    $('#filtro').submit(function(){
        var pagina = $('input[name="page"').val();
        var termo1 = $('.termo1').val();
        var termo2 = $('.termo2').val();

        termo1 = (termo1) ? termo1+'/' : '';
        termo2 = (termo2) ? termo2+'/' : '';


        window.location.href = url_site+pagina+'/busca/'+termo1+'/'+termo2
        return false;
    })
    
    $('.termo1, .termo2').on('keyup focusout change',function(){
        var termo1 = $('.termo1').val();
        var termo2 = $('.termo2').val();
    
        if(termo1 || termo2){
            $('button[type=submit]').prop('disabled', false);
        }else{
            $('button[type=submit]').prop('disabled', true);
        }
      
    })

    //mascaras
    $('input[name=cpf]').mask('999.999.999-99', {reverse:true});

    var options = {
        
    }
    
});

function myAlert(tipo, mensagem, pai, url) {
    url = (url == undefined) ? url == '' : url = url;
    componente = '<div class="alert alert-' + tipo + '" role="alert">' + mensagem + '</div>';

    $(pai).prepend(componente);

    setTimeout(function () {
        $(pai).find('div.alert').remove()
        //redirecionar?
        if (tipo == 'success' && url) {
            setTimeout(function () {
                window.location.href = url;
            }, 500);
        }
    }, 2000)
}