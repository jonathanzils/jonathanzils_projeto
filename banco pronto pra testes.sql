-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.4.22-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Copiando estrutura do banco de dados para jz_sistema
DROP DATABASE IF EXISTS `jz_sistema`;
CREATE DATABASE IF NOT EXISTS `jz_sistema` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `jz_sistema`;

-- Copiando estrutura para tabela jz_sistema.jz_adm
DROP TABLE IF EXISTS `jz_adm`;
CREATE TABLE IF NOT EXISTS `jz_adm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeDaAdm` varchar(255) DEFAULT NULL,
  `cnpj` varchar(255) DEFAULT NULL,
  `dataCadastro` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela jz_sistema.jz_adm: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `jz_adm` DISABLE KEYS */;
INSERT INTO `jz_adm` (`id`, `nomeDaAdm`, `cnpj`, `dataCadastro`) VALUES
	(1, 'ADMINISTRA', '12457898653212', '2022-03-30 12:52:26'),
	(2, 'Killowpar', '65984512326598', '2022-03-30 12:52:45'),
	(3, 'antiadm', '54895632214598', '2022-03-30 12:52:59'),
	(6, 'aaaaaaaaa', '123', '2022-04-01 12:44:54'),
	(7, 'dddd', '46546', '2022-04-06 08:34:22'),
	(8, 'ccc', 'eeee', '2022-04-06 08:34:29'),
	(9, 'dadadadadad', 'aaaaa', '2022-04-07 15:52:41');
/*!40000 ALTER TABLE `jz_adm` ENABLE KEYS */;

-- Copiando estrutura para tabela jz_sistema.jz_bloco
DROP TABLE IF EXISTS `jz_bloco`;
CREATE TABLE IF NOT EXISTS `jz_bloco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `condominio` int(11) DEFAULT 0,
  `nomeDoBloco` varchar(255) NOT NULL DEFAULT '',
  `andar` varchar(255) NOT NULL DEFAULT '',
  `unidades` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `FKcondominio` (`condominio`),
  CONSTRAINT `FKcondominio` FOREIGN KEY (`condominio`) REFERENCES `jz_condominio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela jz_sistema.jz_bloco: ~12 rows (aproximadamente)
/*!40000 ALTER TABLE `jz_bloco` DISABLE KEYS */;
INSERT INTO `jz_bloco` (`id`, `condominio`, `nomeDoBloco`, `andar`, `unidades`) VALUES
	(5, 9, 'A', '4', '4'),
	(6, 18, 'B', '4', '4'),
	(7, 16, 'C', '4', '4'),
	(8, 15, 'D', '4', '4'),
	(9, 13, 'E', '4', '4'),
	(10, 12, 'F', '4', '4'),
	(11, 17, 'G', '4', '4'),
	(12, 10, 'H', '4', '4'),
	(13, 14, 'I', '4', '4'),
	(14, 20, 'J', '4', '4'),
	(15, 10, 'L', '4', '4'),
	(16, 11, 'M', '4', '4');
/*!40000 ALTER TABLE `jz_bloco` ENABLE KEYS */;

-- Copiando estrutura para tabela jz_sistema.jz_condominio
DROP TABLE IF EXISTS `jz_condominio`;
CREATE TABLE IF NOT EXISTS `jz_condominio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeDoCondominio` varchar(255) NOT NULL DEFAULT '',
  `bloco` int(11) DEFAULT NULL,
  `logradouro` varchar(255) DEFAULT '',
  `numero` int(11) DEFAULT 0,
  `bairro` varchar(255) DEFAULT '',
  `cidade` varchar(255) DEFAULT '',
  `estado` varchar(255) DEFAULT '',
  `cep` int(8) DEFAULT 0,
  `adm` int(11) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `FK_jz_condominio_jz_adm` (`adm`),
  CONSTRAINT `FK_jz_condominio_jz_adm` FOREIGN KEY (`adm`) REFERENCES `jz_adm` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela jz_sistema.jz_condominio: ~14 rows (aproximadamente)
/*!40000 ALTER TABLE `jz_condominio` DISABLE KEYS */;
INSERT INTO `jz_condominio` (`id`, `nomeDoCondominio`, `bloco`, `logradouro`, `numero`, `bairro`, `cidade`, `estado`, `cep`, `adm`) VALUES
	(9, 'd', 1, 'd', 1, '', 'd', 'estado', 123, 2),
	(10, 'teste', 1, 'd', 12, 'teste', 'teste', 'PI', 123, 1),
	(11, 'torre do gugu', 1, '9 de novembro', 207, 'rio morto', 'indaial', 'sc ', 1255468, 1),
	(12, 'luziaba', 3, 'imperador loko', 3002, 'tijuca', 'guaruba', 'pr', 4568987, 2),
	(13, 'lalaka', 4, 'boolu', 102, 'centro', 'blumenau', 'sc', 89778456, 2),
	(14, 'taibara', 1, 'koogu', 789, 'talib', 'tokyo', 'ma', 8954, 2),
	(15, 'istambul', 4, 'rua do gueto', 885, 'tuturu', 'talivbra', 'sa', 6560, 3),
	(16, 'ilsamui', 2, 'rua da perdição', 123, 'rio negro', 'solimao', 'am', 546465446, 3),
	(17, 'safari', 1, 'rua torta', 456, 'celesc', 'talibra', 'sc', 321, 3),
	(18, 'girafa azul', 1, 'rua do gridnaldo', 745, 'lepula', 'indail', 'sc', 6746874, 3),
	(19, 'safari invertido', 2, 'rua do flucas', 456, 'lepula do norte', 'timbiense', 'sc', 46546, 2),
	(20, 'tatibana', 1, 'rua dr zimmer', 458, 'centro', 'indaial', 'sc', 5466, 2),
	(46, 'exemplo', 12, 'e', 0, 'asd', 'exemplo', 'AM', 0, 2),
	(48, 'aaaaaaa', 1, 'a', 1, '1', '1', 'AL', 12, 6);
/*!40000 ALTER TABLE `jz_condominio` ENABLE KEYS */;

-- Copiando estrutura para tabela jz_sistema.jz_conselho
DROP TABLE IF EXISTS `jz_conselho`;
CREATE TABLE IF NOT EXISTS `jz_conselho` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `condominio` int(11) NOT NULL DEFAULT 0,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `funcao` enum('sindico','subSindico','conselheiro') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_jz_conselho_jz_condominio` (`condominio`),
  CONSTRAINT `FK_jz_conselho_jz_condominio` FOREIGN KEY (`condominio`) REFERENCES `jz_condominio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela jz_sistema.jz_conselho: ~57 rows (aproximadamente)
/*!40000 ALTER TABLE `jz_conselho` DISABLE KEYS */;
INSERT INTO `jz_conselho` (`id`, `condominio`, `nome`, `funcao`) VALUES
	(26, 9, 'juan', 'subSindico'),
	(27, 9, 'luke', 'conselheiro'),
	(28, 9, 'duda', 'conselheiro'),
	(29, 9, 'dudu', 'conselheiro'),
	(30, 14, 'd', 'conselheiro'),
	(31, 10, 'lulu', 'subSindico'),
	(33, 10, 'dilma1', 'conselheiro'),
	(34, 10, 'dilma2', 'conselheiro'),
	(35, 10, 'dilma3', 'conselheiro'),
	(36, 11, 'dadada', 'conselheiro'),
	(37, 11, 'lukao', 'subSindico'),
	(38, 11, 'Kilonthur', 'conselheiro'),
	(39, 11, 'Ruwuolump', 'conselheiro'),
	(40, 11, 'Falki', 'conselheiro'),
	(41, 12, 'Xazi', 'sindico'),
	(42, 12, 'Qova', 'subSindico'),
	(43, 12, 'Naeclehad', 'conselheiro'),
	(44, 12, 'Nenoy', 'conselheiro'),
	(45, 12, 'Elxyer', 'conselheiro'),
	(46, 13, 'Hiocial', 'sindico'),
	(47, 13, 'Tyogob', 'subSindico'),
	(48, 13, 'Miuru', 'conselheiro'),
	(49, 13, 'Tyami', 'conselheiro'),
	(50, 13, 'Wuium', 'conselheiro'),
	(51, 14, 'Hiolump', 'sindico'),
	(52, 14, 'Xuyrond', 'subSindico'),
	(53, 14, 'Uazwiawyu', 'conselheiro'),
	(54, 14, 'Xosurim', 'conselheiro'),
	(55, 14, 'Reykeibia', 'conselheiro'),
	(57, 15, 'Tyami', 'subSindico'),
	(58, 15, 'Peyhash', 'conselheiro'),
	(59, 15, 'Yamarak', 'conselheiro'),
	(60, 15, 'Miuru', 'conselheiro'),
	(62, 16, 'Goltvocue', 'subSindico'),
	(63, 16, 'Moghvyoleu', 'conselheiro'),
	(64, 16, 'Moghvyoleu', 'conselheiro'),
	(65, 16, 'Dewevey', 'conselheiro'),
	(67, 17, 'Loath', 'subSindico'),
	(68, 17, 'Malbethir', 'conselheiro'),
	(69, 17, 'Wiuvilli', 'conselheiro'),
	(70, 17, 'Dunte', 'conselheiro'),
	(72, 18, 'Iskas', 'subSindico'),
	(73, 18, 'Zoiduge', 'conselheiro'),
	(74, 18, 'Urbuwo', 'conselheiro'),
	(75, 18, 'Zisil', 'conselheiro'),
	(77, 19, 'Wenor', 'subSindico'),
	(78, 19, 'Biewa', 'conselheiro'),
	(79, 19, 'Eizwiin', 'conselheiro'),
	(80, 19, 'Nanoypuo', 'conselheiro'),
	(82, 20, 'Wenor', 'subSindico'),
	(83, 20, 'Kakiunt', 'conselheiro'),
	(84, 20, 'Vonsostili', 'conselheiro'),
	(85, 20, 'Moghvyoleu', 'conselheiro'),
	(86, 18, 'de', 'subSindico'),
	(89, 9, 'ricardao', 'conselheiro'),
	(90, 10, 's', 'sindico'),
	(91, 11, 'teste', 'conselheiro');
/*!40000 ALTER TABLE `jz_conselho` ENABLE KEYS */;

-- Copiando estrutura para tabela jz_sistema.jz_morador
DROP TABLE IF EXISTS `jz_morador`;
CREATE TABLE IF NOT EXISTS `jz_morador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `condominio` int(11) DEFAULT 0,
  `bloco` int(11) DEFAULT 0,
  `unidade` int(11) DEFAULT 0,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `cpf` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `telefone` int(11) DEFAULT 0,
  `dataCadastro` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `keyCondominio` (`condominio`),
  KEY `keyBloco` (`bloco`),
  KEY `keyUnidade` (`unidade`),
  CONSTRAINT `keyBloco` FOREIGN KEY (`bloco`) REFERENCES `jz_bloco` (`id`),
  CONSTRAINT `keyCondominio` FOREIGN KEY (`condominio`) REFERENCES `jz_condominio` (`id`),
  CONSTRAINT `keyUnidade` FOREIGN KEY (`unidade`) REFERENCES `jz_unidade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela jz_sistema.jz_morador: ~10 rows (aproximadamente)
/*!40000 ALTER TABLE `jz_morador` DISABLE KEYS */;
INSERT INTO `jz_morador` (`id`, `condominio`, `bloco`, `unidade`, `nome`, `cpf`, `email`, `telefone`, `dataCadastro`) VALUES
	(5, 9, 5, 8, 'jaoaa', '123.456.789-50', 'contato@zils', 21458796, '2022-04-01 10:33:09'),
	(9, 13, 9, 12, 'thomas', '123.456.789-50', 'contato@zils', 21458796, '2022-04-01 10:27:53'),
	(13, 14, 13, 16, 'laiz', '123.456.789-50', 'contato@zils', 21458796, '2022-04-01 10:27:51'),
	(15, 10, 15, 18, 'tania', '123.456.789-50', 'contato@zils', 21458796, '2022-04-01 10:27:50'),
	(16, 11, 16, 19, 'marco veii', '123.456.789-50', 'contato@zils', 21458796, '2022-04-01 10:27:57'),
	(27, 14, 13, 16, 'zils', '456748', 'vish.bigodao@hotmail.com', 1325698, '2022-04-04 11:43:03'),
	(28, 14, 13, 16, 'zils', '456748', 'vish.bigodao@hotmail.com', 1325698, '2022-04-04 11:43:03'),
	(31, 14, 13, 16, 'zils', '456748', 'vish.bigodao@hotmail.com', 1325698, '2022-04-04 11:43:03'),
	(32, 14, 13, 16, 'zils', '456748', 'vish.bigodao@hotmail.com', 1325698, '2022-04-04 11:43:03'),
	(33, 14, 13, 16, 'zils', '456748', 'vish.bigodao@hotmail.com', 1325698, '2022-04-04 11:43:03');
/*!40000 ALTER TABLE `jz_morador` ENABLE KEYS */;

-- Copiando estrutura para tabela jz_sistema.jz_pets
DROP TABLE IF EXISTS `jz_pets`;
CREATE TABLE IF NOT EXISTS `jz_pets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeDoPet` varchar(50) DEFAULT '',
  `tipo` enum('Cachorro','Gato','Passarinho') DEFAULT NULL,
  `morador` int(11) DEFAULT 0,
  `dataCadastro` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `FK_jz_pets_jz_morador` (`morador`),
  CONSTRAINT `FK_jz_pets_jz_morador` FOREIGN KEY (`morador`) REFERENCES `jz_morador` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela jz_sistema.jz_pets: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `jz_pets` DISABLE KEYS */;
INSERT INTO `jz_pets` (`id`, `nomeDoPet`, `tipo`, `morador`, `dataCadastro`) VALUES
	(5, 'toto', 'Cachorro', 5, '2022-03-31 08:46:53'),
	(6, 'kaka', 'Gato', 13, '2022-03-31 08:46:58'),
	(7, 'bilu', 'Passarinho', 16, '2022-03-31 08:47:22'),
	(8, 'biluli', 'Cachorro', 15, '2022-03-31 08:47:25'),
	(9, 'ziko', 'Cachorro', 9, '2022-03-31 11:21:40');
/*!40000 ALTER TABLE `jz_pets` ENABLE KEYS */;

-- Copiando estrutura para tabela jz_sistema.jz_reservasalaodefesta
DROP TABLE IF EXISTS `jz_reservasalaodefesta`;
CREATE TABLE IF NOT EXISTS `jz_reservasalaodefesta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tituloEvento` varchar(255) DEFAULT '',
  `fUnidade` int(11) DEFAULT NULL,
  `dataDoEvento` timestamp NULL DEFAULT NULL,
  `dataCadastro` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_jz_reservasalaodefesta_jz_unidade` (`fUnidade`),
  CONSTRAINT `FK_jz_reservasalaodefesta_jz_unidade` FOREIGN KEY (`fUnidade`) REFERENCES `jz_unidade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela jz_sistema.jz_reservasalaodefesta: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `jz_reservasalaodefesta` DISABLE KEYS */;
INSERT INTO `jz_reservasalaodefesta` (`id`, `tituloEvento`, `fUnidade`, `dataDoEvento`, `dataCadastro`) VALUES
	(1, 'niver do jao', 8, '2022-04-05 15:08:31', '2022-03-30 15:08:51'),
	(2, 'niver do paulo', 9, '2022-04-10 12:10:10', '2022-03-30 15:09:25'),
	(3, 'quebra pau', 10, '2022-05-10 17:09:41', '2022-03-30 15:09:53');
/*!40000 ALTER TABLE `jz_reservasalaodefesta` ENABLE KEYS */;

-- Copiando estrutura para tabela jz_sistema.jz_unidade
DROP TABLE IF EXISTS `jz_unidade`;
CREATE TABLE IF NOT EXISTS `jz_unidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `condominio` int(11) DEFAULT 0,
  `bloco` int(11) DEFAULT 0,
  `nomeDaUnidade` varchar(255) NOT NULL DEFAULT '',
  `metragem` int(11) NOT NULL DEFAULT 0,
  `vagas` int(11) NOT NULL DEFAULT 0,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `chCondominio` (`condominio`),
  KEY `chBloco` (`bloco`),
  CONSTRAINT `chBloco` FOREIGN KEY (`bloco`) REFERENCES `jz_bloco` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela jz_sistema.jz_unidade: ~12 rows (aproximadamente)
/*!40000 ALTER TABLE `jz_unidade` DISABLE KEYS */;
INSERT INTO `jz_unidade` (`id`, `condominio`, `bloco`, `nomeDaUnidade`, `metragem`, `vagas`, `dataCadastro`) VALUES
	(8, 9, 5, '102', 23, 1, '2022-03-30 13:34:21'),
	(9, 10, 6, '103', 23, 1, '2022-03-30 13:36:26'),
	(10, 11, 7, '104', 23, 1, '2022-03-30 13:36:31'),
	(11, 12, 8, '104', 23, 1, '2022-03-30 13:36:44'),
	(12, 13, 9, '104', 23, 1, '2022-03-30 13:36:41'),
	(13, 14, 10, '102', 23, 1, '2022-03-30 13:35:50'),
	(14, 15, 11, '102', 23, 1, '2022-03-30 13:35:53'),
	(15, 16, 12, '102', 23, 1, '2022-03-30 13:35:57'),
	(16, 17, 13, '102', 23, 1, '2022-03-30 13:36:01'),
	(17, 18, 14, '102', 23, 1, '2022-03-30 13:36:04'),
	(18, 19, 15, '102', 23, 1, '2022-03-30 13:36:07'),
	(19, 10, 10, '10222', 23, 1, '2022-04-01 09:57:10');
/*!40000 ALTER TABLE `jz_unidade` ENABLE KEYS */;

-- Copiando estrutura para tabela jz_sistema.jz_usuarios
DROP TABLE IF EXISTS `jz_usuarios`;
CREATE TABLE IF NOT EXISTS `jz_usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `usuario` varchar(255) DEFAULT NULL,
  `senha` varchar(255) DEFAULT NULL,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela jz_sistema.jz_usuarios: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `jz_usuarios` DISABLE KEYS */;
INSERT INTO `jz_usuarios` (`id`, `nome`, `usuario`, `senha`, `dataCadastro`) VALUES
	(1, 'Zils', 'jo', '202cb962ac59075b964b07152d234b70', '2022-04-07 08:14:57');
/*!40000 ALTER TABLE `jz_usuarios` ENABLE KEYS */;

-- Copiando estrutura para tabela jz_sistema.listadeconvidados
DROP TABLE IF EXISTS `listadeconvidados`;
CREATE TABLE IF NOT EXISTS `listadeconvidados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `convidado` varchar(255) NOT NULL DEFAULT '',
  `cpf` varchar(255) NOT NULL DEFAULT '',
  `celular` int(11) NOT NULL DEFAULT 0,
  `fReservaSalao` int(11) NOT NULL DEFAULT 0,
  `fUnidade` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `FK_listadeconvidados_jz_reservasalaodefesta` (`fReservaSalao`),
  KEY `FK_listadeconvidados_jz_unidade` (`fUnidade`),
  CONSTRAINT `FK_listadeconvidados_jz_reservasalaodefesta` FOREIGN KEY (`fReservaSalao`) REFERENCES `jz_reservasalaodefesta` (`id`),
  CONSTRAINT `FK_listadeconvidados_jz_unidade` FOREIGN KEY (`fUnidade`) REFERENCES `jz_unidade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela jz_sistema.listadeconvidados: ~24 rows (aproximadamente)
/*!40000 ALTER TABLE `listadeconvidados` DISABLE KEYS */;
INSERT INTO `listadeconvidados` (`id`, `convidado`, `cpf`, `celular`, `fReservaSalao`, `fUnidade`) VALUES
	(1, 'jao', '123.123.123-30', 91214233, 1, 8),
	(2, 'maria', '495.746.870-23', 91214233, 1, 8),
	(3, 'dudu', '549.187.660-72', 91214233, 1, 8),
	(4, 'liko', '738.046.980-00', 91214233, 1, 8),
	(5, 'lepo', '935.412.830-09', 91214233, 1, 8),
	(6, 'dani', '356.822.010-51', 91214233, 1, 8),
	(7, 'mark', '171.313.040-85', 91214233, 1, 8),
	(8, 'john', '302.751.800-34', 91214233, 1, 8),
	(11, 'paulo', '302.751.456-34', 91214233, 2, 9),
	(12, 'karl', '333.266.700-95', 91214233, 2, 9),
	(13, 'juan', '515.871.080-54', 91214233, 2, 9),
	(14, 'jean', '016.592.070-02', 91214233, 2, 9),
	(15, 'deku', '031.875.340-58', 91214233, 2, 9),
	(16, 'midoria', '987.993.550-00', 91214233, 2, 9),
	(17, 'tanjiro', '205.267.700-31', 91214233, 2, 9),
	(18, 'nezuko', '770.201.530-62', 91214233, 2, 9),
	(19, 'lula', '352.572.450-00', 91214233, 3, 10),
	(20, 'bolsonojo', '704.039.550-92', 91214233, 3, 10),
	(21, 'bolsominion1', '246.610.280-31', 91214233, 3, 10),
	(22, 'bolsominion2', '036.539.010-06', 91214233, 3, 10),
	(23, 'bolsominion3', '287.998.340-14', 91214233, 3, 10),
	(24, 'lulaminion1', '049.654.240-00', 91214233, 3, 10),
	(25, 'lulaminion2', '934.038.630-26', 91214233, 3, 10),
	(26, 'lulaminion3', '686.882.380-06', 91214233, 3, 10);
/*!40000 ALTER TABLE `listadeconvidados` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
