<?
include('uteis.php');

//session_destroy();
?>
<!DOCTYPE html>
<html lang="pt-BR" class="bg-dark">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="icofont/icofont.min.css">
    <title>projeto</title>

<body class="bg-dark">
    <main class="container">
        <div class="col-4 text-center login">
            <h1>Efetue o login</h1>
        <form action="<?=$url_site?>/controllers/restrito.php" method="POST">
            <div class="mb-3 col-12 mt-5"> 
                <input type="text" class="form-control"name="usuario" placeholder="Login">
            </div>
            <div class="mb-3 col-12">
                <input type="password" name="senha" class="form-control"placeholder="senha">
            </div>
            <button type="submit" class="btn btn-primary col-11">Logar</button>
        </form>
        </div>
        
        <script src="js/jquery-3.6.0.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
        <script src="js/app.js"></script>
        <?if(isset($_GET['msg'])){?>
            <script type="text/javascript">
                $(function(){
                    myAlert('danger','<?=$_GET['msg']?>','main')
                })
                </script>
<?}?>

</main>
</body>

</html>